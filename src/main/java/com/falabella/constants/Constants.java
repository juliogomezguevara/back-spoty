package com.falabella.constants;

import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class Constants {
	public final static String TOKEN_URI = "https://accounts.spotify.com/api/token?";
	public final static HttpHeaders TOKEN_URI_HEADERS = getTokenHeaders();
	public final static MultiValueMap<String, String> TOKEN_URI_BODY = getTokenBody();
	public final static String SEARCH_URI = "https://api.spotify.com/v1/search?q=%s&type=album";
	
	public static HttpHeaders getSearchHeaders(String token) {
		HttpHeaders headers = new HttpHeaders();		
		headers.add("Authorization", "Bearer " + token);
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");
		
		return headers;
	}
	
	private static HttpHeaders getTokenHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		
		return headers;
	}
	
	private static MultiValueMap<String, String> getTokenBody(){
		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
		body.add("client_id", "5de5cc1dea9a49248447e9c1fc8c883e");
		body.add("client_secret", "f96497e6b670460a8b68279f9d9a1375");
		body.add("grant_type", "client_credentials");
		return body;
	}
}
