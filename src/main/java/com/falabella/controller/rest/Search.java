package com.falabella.controller.rest;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.falabella.constants.Constants;
import com.falabella.security.AccessToken;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
public class Search {

	@RequestMapping("/search/{name}")
	public ResponseEntity<String> searchAlbum(@PathVariable String name) throws JsonParseException, JsonMappingException, IOException  {
		String token = AccessToken.getAccessToken();
		
		HttpEntity<?> entity = new HttpEntity<>(Constants.getSearchHeaders(token));
		
		return new RestTemplate().exchange(String.format(Constants.SEARCH_URI, name), HttpMethod.GET, entity, String.class);
	}
}
