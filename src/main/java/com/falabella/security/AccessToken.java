package com.falabella.security;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import com.falabella.constants.Constants;
import com.falabella.jsons.Token;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccessToken {

	private static String accessToken;

	private AccessToken() {
	}

	public static String getAccessToken() throws JsonParseException, JsonMappingException, IOException {
		if (AccessToken.accessToken != null) {
			return AccessToken.accessToken;
		}

		return setAccessToken();
	}

	private static String setAccessToken() throws JsonParseException, JsonMappingException, IOException {
		HttpEntity<?> entity = new HttpEntity<Object>(Constants.TOKEN_URI_BODY, Constants.TOKEN_URI_HEADERS);

		String json = new RestTemplate().postForObject(Constants.TOKEN_URI, entity, String.class);

		AccessToken.accessToken = new ObjectMapper().readValue(json, Token.class).getAccessToken();

		return AccessToken.accessToken;
	}

}
